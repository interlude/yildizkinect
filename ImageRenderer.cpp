﻿//------------------------------------------------------------------------------
// <copyright file="ImageRenderer.cpp" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

#include "stdafx.h"
#include "ImageRenderer.h"
#include <d2d1helper.h>
#include <ctime>
#include <wchar.h>
/// <summary>
/// Constructor
/// </summary>
ImageRenderer::ImageRenderer() : 
m_hWnd(0),
	m_sourceWidth(0),
	m_sourceHeight(0),
	m_sourceStride(0),
	m_pD2DFactory(NULL), 
	m_pRenderTarget(NULL),
	m_pBitmap(0),
	m_pIwicImagingFactory(NULL),
	m_pRT(NULL)
{
}

/// <summary>
/// Destructor
/// </summary>
ImageRenderer::~ImageRenderer()
{
	DiscardResources();
	SafeRelease(m_pD2DFactory);
}


HRESULT ImageRenderer::SaveByteArrayAsBitmap(BYTE* parray, int width, int height){
	SYSTEMTIME localTime;
	GetLocalTime(&localTime);

	wchar_t date[50];
	wchar_t time[50];
	wchar_t path[50] = L"C:\\kinect\\id2d1\\btmp";
	swprintf(date,L"%02d_%02d_%02d",localTime.wDay, localTime.wMonth, localTime.wYear);
	swprintf(time,L"%02d_%02d_%02d-%02d",localTime.wHour, localTime.wMinute, localTime.wSecond, localTime.wMilliseconds);
	
	
	wchar_t filename[200];
	swprintf(filename,L"%s_%s-%s.jpeg",path,date,time);

	HRESULT hr;
	hr = m_pIwicImagingFactory->CreateStream(&m_pWICStream);
	IPropertyBag2 *pPropertybag = NULL;
	if(SUCCEEDED(hr)){
		hr = m_pWICStream->InitializeFromFilename(filename, 
			GENERIC_WRITE);
		
	}

	if(SUCCEEDED(hr)){
		hr  = m_pIwicImagingFactory->CreateEncoder(GUID_ContainerFormatJpeg,
			NULL,
			&m_pWICEncoder);
	}

	if (SUCCEEDED(hr))
	{
		hr = m_pWICEncoder->Initialize(m_pWICStream,WICBitmapEncoderNoCache);
	}

	if (SUCCEEDED(hr))
	{
		hr = m_pWICEncoder->CreateNewFrame(&m_piBitmapFrameEncode, &pPropertybag);
	}



	if (SUCCEEDED(hr))
	{
		hr = m_piBitmapFrameEncode->Initialize(pPropertybag);
	}


	if (SUCCEEDED(hr))
	{
		hr = m_piBitmapFrameEncode->SetSize(width, height);
	}
	if (SUCCEEDED(hr))
	{
		WICPixelFormatGUID format = GUID_WICPixelFormat24bppBGR;
		

		hr = m_piBitmapFrameEncode->SetPixelFormat(&format);
	}

	if (SUCCEEDED(hr))
	{
		UINT cbStride = width *3; /***WICGetStride***/;
		UINT cbBufferSize = height * cbStride;

		if(parray != NULL){

			hr = m_piBitmapFrameEncode->WritePixels(height, cbStride, cbBufferSize, parray);
			
		}

	}

	if (SUCCEEDED(hr))
	{
		hr = m_piBitmapFrameEncode->Commit();
	}
	if (SUCCEEDED(hr))
	{
		hr = m_pWICEncoder->Commit();
	}

	/*if(m_piBitmapFrameEncode)
		m_piBitmapFrameEncode->Release();

	if(m_pWICStream)
		m_pWICStream->Release();*/

	return hr;
}

/* id2d1 bitmap'ten wicbitmap oluşturmak için yazılmıştı, çalışmıyor.
HRESULT	ImageRenderer::SaveBitmaptoFile(ID2D1Bitmap* pId2d1Bitmap){
	//Getting width and height of ID2D1Bitmap
	UINT sc_bitmapWidth		= pId2d1Bitmap->GetPixelSize().width;
	UINT sc_bitmapHeight	= pId2d1Bitmap->GetPixelSize().height;

	//Creating an WIC bitmap
	HRESULT hr = m_pIwicImagingFactory->CreateBitmap(
		sc_bitmapWidth,
		sc_bitmapHeight,
		GUID_WICPixelFormat32bppPBGRA,
		WICBitmapCacheOnLoad,
		&m_pWICBitmap
		);

	D2D1_RENDER_TARGET_PROPERTIES wicrtProps = D2D1::RenderTargetProperties();

	wicrtProps.pixelFormat = D2D1::PixelFormat(DXGI_FORMAT_B8G8R8A8_UNORM ,D2D1_ALPHA_MODE_PREMULTIPLIED);
	wicrtProps.type = D2D1_RENDER_TARGET_TYPE_DEFAULT;
	wicrtProps.usage = D2D1_RENDER_TARGET_USAGE_NONE;


	if(SUCCEEDED(hr)){
		//Creating a render target using D2DFactory
		hr = m_pD2DFactory->CreateWicBitmapRenderTarget(
			m_pWICBitmap,
			wicrtProps,
			&m_pRT
			);}


	if(SUCCEEDED(hr)){
		m_pRT->BeginDraw();

		m_pRT->DrawBitmap(pId2d1Bitmap);

		//hr = m_pRT->EndDraw();
	}

	if(SUCCEEDED(hr))
	{
		hr = m_pIwicImagingFactory->CreateStream(&m_pWICStream);

	}

	WICPixelFormatGUID format = GUID_WICPixelFormat32bppPBGRA;

	if(SUCCEEDED(hr)){
		hr = m_pWICStream->InitializeFromFilename(L"C:\\kinect\\id2d1\\bitmap.png", 
			GENERIC_WRITE);

	}

	if(SUCCEEDED(hr)){
		hr  = m_pIwicImagingFactory->CreateEncoder(GUID_ContainerFormatPng,
			NULL,
			&m_pWICEncoder);
	}



	if (SUCCEEDED(hr))
	{
		hr = m_pWICEncoder->Initialize(m_pWICStream, WICBitmapEncoderNoCache);
	}
	if (SUCCEEDED(hr))
	{
		hr = m_pWICEncoder->CreateNewFrame(&m_piBitmapFrameEncode, NULL);
	}
	if (SUCCEEDED(hr))
	{
		hr = m_piBitmapFrameEncode->Initialize(NULL);
	}

	if (SUCCEEDED(hr))
	{
		hr = m_piBitmapFrameEncode->SetSize(sc_bitmapWidth, sc_bitmapHeight);
	}
	if (SUCCEEDED(hr))
	{
		hr = m_piBitmapFrameEncode->SetPixelFormat(&format);
	}
	if (SUCCEEDED(hr))
	{
		hr = m_piBitmapFrameEncode->WriteSource(m_pWICBitmap, NULL);
	}
	if (SUCCEEDED(hr))
	{
		hr = m_piBitmapFrameEncode->Commit();
	}
	if (SUCCEEDED(hr))
	{
		hr = m_pWICEncoder->Commit();
	}

	return hr;
}
*/
/// <summary>
/// Ensure necessary Direct2d resources are created
/// </summary>
/// <returns>indicates success or failure</returns>
HRESULT ImageRenderer::EnsureResources()
{
	HRESULT hr = S_OK;

	if (NULL == m_pRenderTarget)
	{
		D2D1_SIZE_U size = D2D1::SizeU(m_sourceWidth, m_sourceHeight);

		D2D1_RENDER_TARGET_PROPERTIES rtProps = D2D1::RenderTargetProperties();
		rtProps.pixelFormat = D2D1::PixelFormat(DXGI_FORMAT_B8G8R8A8_UNORM, D2D1_ALPHA_MODE_IGNORE);
		rtProps.usage = D2D1_RENDER_TARGET_USAGE_GDI_COMPATIBLE;

		// Create a hWnd render target, in order to render to the window set in initialize
		hr = m_pD2DFactory->CreateHwndRenderTarget(
			rtProps,
			D2D1::HwndRenderTargetProperties(m_hWnd, size),
			&m_pRenderTarget
			);

		if ( FAILED(hr) )
		{
			return hr;
		}

		// Create a bitmap that we can copy image data into and then render to the target
		hr = m_pRenderTarget->CreateBitmap(
			size, 
			D2D1::BitmapProperties( D2D1::PixelFormat( DXGI_FORMAT_B8G8R8A8_UNORM, D2D1_ALPHA_MODE_IGNORE) ),
			&m_pBitmap 
			);

		if ( FAILED(hr) )
		{
			SafeRelease(m_pRenderTarget);
			return hr;
		}
	}

	return hr;
}

/// <summary>
/// Dispose of Direct2d resources 
/// </summary>
void ImageRenderer::DiscardResources()
{
	SafeRelease(m_pRenderTarget);
	SafeRelease(m_pBitmap);
}

/// <summary>
/// Set the window to draw to as well as the video format
/// Implied bits per pixel is 32
/// </summary>
/// <param name="hWnd">window to draw to</param>
/// <param name="pD2DFactory">already created D2D factory object</param>
/// <param name="sourceWidth">width (in pixels) of image data to be drawn</param>
/// <param name="sourceHeight">height (in pixels) of image data to be drawn</param>
/// <param name="sourceStride">length (in bytes) of a single scanline</param>
/// <returns>indicates success or failure</returns>
HRESULT ImageRenderer::Initialize(HWND hWnd, ID2D1Factory* pD2DFactory, int sourceWidth, int sourceHeight, int sourceStride)
{
	if (NULL == pD2DFactory)
	{
		return E_INVALIDARG;
	}


	// Initialize COM
	CoInitialize(NULL);

	m_pIwicImagingFactory = NULL;
	HRESULT hr;

	hr = CoCreateInstance(CLSID_WICImagingFactory,
		NULL,
		CLSCTX_INPROC_SERVER,
		IID_IWICImagingFactory,
		(LPVOID*)&m_pIwicImagingFactory);


	m_hWnd = hWnd;

	// One factory for the entire application so save a pointer here
	m_pD2DFactory = pD2DFactory;

	m_pD2DFactory->AddRef();

	// Get the frame size
	m_sourceWidth  = sourceWidth;
	m_sourceHeight = sourceHeight;
	m_sourceStride = sourceStride;

	return S_OK;
}

/// <summary>
/// Draws a 32 bit per pixel image of previously specified width, height, and stride to the associated hwnd
/// </summary>
/// <param name="pImage">image data in RGBX format</param>
/// <param name="cbImage">size of image data in bytes</param>
/// <returns>indicates success or failure</returns>
HRESULT ImageRenderer::Draw(BYTE* pImage, unsigned long cbImage)
{
	// incorrectly sized image data passed in
	if ( cbImage < ((m_sourceHeight - 1) * m_sourceStride) + (m_sourceWidth * 4) )
	{
		return E_INVALIDARG;
	}

	// create the resources for this draw device
	// they will be recreated if previously lost
	HRESULT hr = EnsureResources();

	if ( FAILED(hr) )
	{
		return hr;
	}

	// Copy the image that was passed in into the direct2d bitmap
	hr = m_pBitmap->CopyFromMemory(NULL, pImage, m_sourceStride);

	if ( FAILED(hr) )
	{
		return hr;
	}

	m_pRenderTarget->BeginDraw();

	// Draw the bitmap stretched to the size of the window
	m_pRenderTarget->DrawBitmap(m_pBitmap);


	hr = m_pRenderTarget->EndDraw();
	//SaveBitmaptoFile(m_pBitmap);
	// Device lost, need to recreate the render target
	// We'll dispose it now and retry drawing
	if (hr == D2DERR_RECREATE_TARGET)
	{
		hr = S_OK;
		DiscardResources();
	}

	return hr;
}